-- Table: public.customer

-- DROP TABLE IF EXISTS public.customer;

CREATE TABLE IF NOT EXISTS public.customer
(
    customer_id uuid NOT NULL,
    email character varying COLLATE pg_catalog."default" NOT NULL,
    name character varying COLLATE pg_catalog."default",
    firstname character varying COLLATE pg_catalog."default",
    adress character varying COLLATE pg_catalog."default",
    CONSTRAINT customer_pkey PRIMARY KEY (customer_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.customer
    OWNER to postgres;


-- Table: public.book

-- DROP TABLE IF EXISTS public.book;

CREATE TABLE IF NOT EXISTS public.book
(
    book_id uuid NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL,
    description character varying COLLATE pg_catalog."default",
    price numeric(10,2),
    CONSTRAINT book_pkey PRIMARY KEY (book_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.book
    OWNER to postgres;

-- Ajout du livre
INSERT INTO public.book (book_id, name, description, price) VALUES (
'd47e1796-822d-11ee-b962-0242ac120002', 'Le petit prince', 'Livre le petit prince', '7')
 returning book_id;

-- Ajout du client
 INSERT INTO public.customer (customer_id, email, name, firstname) VALUES (
'848693c6-822d-11ee-b962-0242ac120002', 'JDoe@exalt.com', 'DOE', 'John')
 returning customer_id;

 
-- Table: public.book_orders

-- DROP TABLE IF EXISTS public.book_orders;

CREATE TABLE IF NOT EXISTS public.book_orders
(
    book_orders_id uuid NOT NULL,
    book_id uuid,
    quantity integer,
    price numeric(10,2),
    CONSTRAINT book_orders_pkey PRIMARY KEY (book_orders_id),
    CONSTRAINT book_id FOREIGN KEY (book_id)
        REFERENCES public.book (book_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.book_orders
    OWNER to postgres;
-- Index: fki_book_id

-- DROP INDEX IF EXISTS public.fki_book_id;

CREATE INDEX IF NOT EXISTS fki_book_id
    ON public.book_orders USING btree
    (book_id ASC NULLS LAST)
    TABLESPACE pg_default;


-- Table: public.orders

-- DROP TABLE IF EXISTS public.orders;

CREATE TABLE IF NOT EXISTS public.orders
(
    orders_id uuid NOT NULL,
    date_orders date NOT NULL,
    book_orders_id uuid NOT NULL,
    customer_id uuid NOT NULL,
    CONSTRAINT orders_pkey PRIMARY KEY (orders_id),
    CONSTRAINT customer_id FOREIGN KEY (customer_id)
        REFERENCES public.customer (customer_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT book_orders_id FOREIGN KEY (book_orders_id)
        REFERENCES public.book_orders (book_orders_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.orders
    OWNER to postgres;
-- Index: fki_customer_id

-- DROP INDEX IF EXISTS public.fki_customer_id;

CREATE INDEX IF NOT EXISTS fki_customer_id
    ON public.orders USING btree
    (customer_id ASC NULLS LAST)
    TABLESPACE pg_default;
-- Index: fki_book_orders_id

-- DROP INDEX IF EXISTS public.fki_book_orders_id;

CREATE INDEX IF NOT EXISTS fki_book_orders_id
    ON public.orders USING btree
    (book_orders_id ASC NULLS LAST)
    TABLESPACE pg_default;


-- Passer la orders d'un book
BEGIN;
INSERT INTO public.book_orders (book_orders_id, book_id, quantity, price) VALUES (
    '65040243-4454-4e0d-a9f9-7ba3f2201cfb', 'd47e1796-822d-11ee-b962-0242ac120002', 1,
    (SELECT price from book WHERE book.book_id = 'd47e1796-822d-11ee-b962-0242ac120002')
 ) returning book_orders_id;
INSERT INTO public.orders (orders_id, date_orders, book_orders_id, customer_id) VALUES (
    '2fdd96a6-8b8a-4942-a4f1-2a2faf3e727e', '2023-11-02', '65040243-4454-4e0d-a9f9-7ba3f2201cfb', '848693c6-822d-11ee-b962-0242ac120002'
 ) returning orders_id;
 COMMIT;

 -- Le prix du book a augmenté
UPDATE public.book
SET price = 8.10
WHERE book.book_id = 'd47e1796-822d-11ee-b962-0242ac120002';

-- Passer la orders de 22 orders
BEGIN;
INSERT INTO public.book_orders (book_orders_id, book_id, quantity, price) VALUES (
    '91df5d67-4902-48d1-85ed-31ee6ab15daa', 'd47e1796-822d-11ee-b962-0242ac120002', 22,
    (SELECT price from book WHERE book.book_id = 'd47e1796-822d-11ee-b962-0242ac120002')
 ) returning book_orders_id;
INSERT INTO public.orders (orders_id, date_orders, book_orders_id, customer_id) VALUES (
    'bf894312-8126-425d-84da-3a6d0773736c', '2023-11-09', '91df5d67-4902-48d1-85ed-31ee6ab15daa', '848693c6-822d-11ee-b962-0242ac120002'
 ) returning orders_id;
COMMIT;