**KATA SQL**

Use case :

1. Le lundi, je commande un exemplaire du "Petit Prince". Le jeudi je commande 25 exemplaires supplementaire.

1. Le tarif change entre les 2 commandes. Gérer le prix payé à la date du commande.

![kata-sql](ennonce.png)

1. Quelle(s) table(s) pour réaliser les commandes.

1. Requetes pour retrouver toutes les commandes avec au moins 1 exemplaire du "Petit Prince"
