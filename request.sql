select c.name, c.firstname, o.date_orders, b.name, bo.quantity, bo.price as prix_unitaire_achat
from customer as c
inner join orders as o on c.customer_id = o.customer_id
inner join book_orders as bo on bo.book_orders_id = o.book_orders_id
inner join book as b on b.book_id = bo.book_id
where b.name = 'Le petit prince'